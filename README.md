# CSS Animation
Intern: Nguyen Ngo Lap

## 1. Overview of CSS Transitions and Transforms
### CSS transforms

- Changes the shape and position of the affected content by modifying the coordinate space
- Do not interrupt the normal document flow

**Notes:**

- Transform has stacking order - each transformation will affect the overall look. Each additional transform is compiled on top of the ones that came before it in the list
- To make 3D transforms look right, need to add `perspective`

## 2. Understanding CSS Animations
2 steps to a CSS Animation:

1. Define the animation
2. Assign it to a specific element (or elements)

### The keyframe block
Keyframes are a list describing what should happen over the course of the animation.

**Notes:**

- `animation-fill-mode` - tells the animation what to do outside the actual duration of its animation
    - `backwards` - applies syltes from the first executed keyframe after animation ends
    - `forwards` - applies styles from the last executed keyframe after animation ends
    - `both` - combines the above
    - `none` - none (default)
- `animation-direction` - manipulates what order the keyframes are executed in
    - `normal` - all iterations of the animation played as specified. Keyframes play from start to end
    - `reverse` - all iterations played in the reverse direction
    - `alternate` - animation direction is alternated with eeach iteration of the animation. Keyframes play from start to end, then end to start, and continue alternating
    - `alternate-reverse` - the opposite of the above
- `animation-timing-function` options:
    - Predefined keywords: `ease`, `linear`, `ease-in`, `ease-out`, `ease-in-out`
    - Cubic-bezier functions: `cubic-bezier(x1, y1, x2, y2)`
        - http://cubic-bezier.com
    - Steps

## 3. CSS animation building blocks
**Notes:**

- `animation-play-state` - starts/pauses animation
- `steps` - divides the animation duration into equal parts
    - https://www.smashingmagazine.com/2014/04/understanding-css-timing-functions/#introducing-the-steps-timing-function

## 4. Applying CSS animations to SVG
### SVG

- Scalable and resolution independent
- Small file size
- Accessibility

**Notes:**

- On drawing SVG:
    - Use independent shapes
        - Avoid merging paths
        - Layer shapes instead of knocking them out of the background shapes
    - Keep drawings simple
        - Saves file size
        - Means easier animation
    - Use simple shapes instead of paths
        - When possible, draw with the shape tools instead of making custom paths
    - Live text or outlined text?
        - Decide early
        - Live text is more accessible but may require web fonts to be loaded
        - Outlined text may result in a smaller file size
- **SVG Optimizer:**
    - svgomg
    - Peter Collinridge
- Most useful animatable properties:
    - SVG fill
    - Opacity
    - CSS transforms on SVG elements
- SVG elements' default transform origin: top left of the canvas
- JS library for animating SVG:
    - GreenSock
    - Snap.svg

## 5. Performance, browser support and fallbacks
### When to use CSS animations?

- CSS keyframe animation:
    - Demonstrations or linear animations
    - Very simple state-based animation
- CSS transitions:
    - Toggling between two states
    - Animating on :hover, :active, :checked

### When to use JS?

- Complex state animations
- Dynamic animations
- Physics emulation
- Support older browsers (CSS Animation requires more capable modern browsers)

### What about SVG?

- Vector-based animation
- Icons or other self-contained animations

### Team combination

- Combine CSS, JS, and SVG
- JS for the logic, CSS for the motion

*(?) Consider the content, the animation's role and the audience to choose the best options*

### Performance
Browser render different animation properties with different efficiency.

\- Most performant properties

- Opacity
- Scale, rotation, position with transform

\- The 4 steps browsers have to go through to update changes on the page:

- Style: Calculating the style to apply
- Layout: Generating the geometry and position of each element
- Paint: Filling out the pixels for each element into the layers that make up the page
- Composite: Drawing out the layers onto the screen

\- Reference:

- Which steps the properties affect: https://csstriggers.com/
- https://www.html5rocks.com/en/tutorials/speed/high-performance-animations/

\- Tricks to improve performance:

- The translate 3D "hack"
    - Turns on hardware acceleration
    - Promotes objects to a new layer -> forces browsers to increase performance
    - Transform: `translatẻd(0, 0, 0)`
- The will-change property
    - Notifies browsers that an object will be animating for priority rendering
    - More fine-grained control than the translate3d "hack"

### Code organization
*Depends. Personal style?*
- Keep object specific animation code with the object's other styles
- Centralize animation code that will be repurposed on multiple objects throughout a project

## 6. Tools

Animation:

- Popular timing function: http://easings.net/
- Make own complex animations, using matrix transforms: http://bouncejs.com/

SASS:

- Sass tutorials: http://thesassway.com/

Understanding SVG:

- https://www.sarasoueidan.com/blog/svg-coordinate-systems/

INSPIRATION!

- https://codepen.io/